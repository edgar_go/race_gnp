# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_11_15_181109) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cities", force: :cascade do |t|
    t.string "slug"
    t.string "city"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "cities_promotions", force: :cascade do |t|
    t.bigint "promotions_id"
    t.bigint "cities_id"
    t.index ["cities_id"], name: "index_cities_promotions_on_cities_id"
    t.index ["promotions_id"], name: "index_cities_promotions_on_promotions_id"
  end

  create_table "data_medics", force: :cascade do |t|
    t.string "slug"
    t.bigint "runner_id"
    t.string "type_bold"
    t.boolean "chronic_disease"
    t.string "chronic_disease_des"
    t.string "emergency_contact_name"
    t.string "emergency_contact_tel"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["runner_id"], name: "index_data_medics_on_runner_id"
  end

  create_table "inscriptions", force: :cascade do |t|
    t.string "slug"
    t.string "folio"
    t.bigint "race_id"
    t.integer "race_folio"
    t.bigint "runner_id"
    t.boolean "terms_and_conditions"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["race_id"], name: "index_inscriptions_on_race_id"
    t.index ["runner_id"], name: "index_inscriptions_on_runner_id"
  end

  create_table "payments", force: :cascade do |t|
    t.string "slug"
    t.bigint "inscription_id"
    t.float "mount"
    t.string "type_payment"
    t.string "status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["inscription_id"], name: "index_payments_on_inscription_id"
  end

  create_table "personalizations", force: :cascade do |t|
    t.string "slug"
    t.bigint "runner_id"
    t.string "personalitation"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["runner_id"], name: "index_personalizations_on_runner_id"
  end

  create_table "promotion_types", force: :cascade do |t|
    t.string "slug"
    t.string "folio"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "promotions", force: :cascade do |t|
    t.string "slug"
    t.string "name"
    t.string "description"
    t.bigint "promotion_type_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["promotion_type_id"], name: "index_promotions_on_promotion_type_id"
  end

  create_table "races", force: :cascade do |t|
    t.string "slug"
    t.string "folio"
    t.string "name"
    t.bigint "subsidiary_id"
    t.string "distance"
    t.integer "quota"
    t.integer "disponibility"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["subsidiary_id"], name: "index_races_on_subsidiary_id"
  end

  create_table "runners", force: :cascade do |t|
    t.string "slug"
    t.string "runner_folio"
    t.string "name"
    t.string "paternal"
    t.string "maternal"
    t.string "email"
    t.string "date_birthday"
    t.boolean "notification_email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "subsidiaries", force: :cascade do |t|
    t.string "slug"
    t.string "serial"
    t.string "name"
    t.integer "filial_sede_id"
    t.string "representantive"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "subsidiaries_cities", force: :cascade do |t|
    t.bigint "cities_id"
    t.bigint "subsidiaries_id"
    t.index ["cities_id"], name: "index_subsidiaries_cities_on_cities_id"
    t.index ["subsidiaries_id"], name: "index_subsidiaries_cities_on_subsidiaries_id"
  end

  add_foreign_key "data_medics", "runners"
  add_foreign_key "inscriptions", "races"
  add_foreign_key "inscriptions", "runners"
  add_foreign_key "payments", "inscriptions"
  add_foreign_key "personalizations", "runners"
  add_foreign_key "promotions", "promotion_types"
  add_foreign_key "races", "subsidiaries"
end
