class CreateRunners < ActiveRecord::Migration[6.0]

  def change
    create_table :runners do |t|
      t.string  :slug
      t.string  :runner_folio
      t.string  :name 
      t.string  :paternal
      t.string  :maternal
      t.string  :email
      t.string  :date_birthday
      t.boolean :notification_email      
      t.timestamps
    end
  end
end
