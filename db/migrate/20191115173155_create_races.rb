class CreateRaces < ActiveRecord::Migration[6.0]
  def change
    create_table :races do |t|
      t.string  :slug
      t.string  :folio
      t.string  :name
      t.references :subsidiary, foreign_key: true
      t.string  :distance
      t.integer :quota
      t.integer :disponibility
      t.timestamps
    end
  end
end
