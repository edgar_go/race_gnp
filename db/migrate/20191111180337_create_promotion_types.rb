class CreatePromotionTypes < ActiveRecord::Migration[6.0]
  def change
    create_table :promotion_types do |t|
      t.string :slug
      t.string :folio
      t.string :name
      t.timestamps
    end
  end
end
