class CreateDataMedics < ActiveRecord::Migration[6.0]
  def change
    create_table :data_medics do |t|
      t.string  :slug
      t.references :runner, foreign_key: true
      t.string  :type_bold
      t.boolean :chronic_disease      
      t.string  :chronic_disease_des
      t.string  :emergency_contact_name
      t.string  :emergency_contact_tel
      t.timestamps
    end
  end
end
