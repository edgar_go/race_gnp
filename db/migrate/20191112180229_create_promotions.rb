class CreatePromotions < ActiveRecord::Migration[6.0]
  def change
    create_table :promotions do |t|
      t.string :slug
      t.string :name
      t.string :description
      t.references :promotion_type, foreign_key: true

      t.timestamps
    end
  end
end
