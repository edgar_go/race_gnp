class CreatePayments < ActiveRecord::Migration[6.0]
  def change
    create_table :payments do |t|
      t.string :slug
      t.references :inscription, foreign_key: true
      t.float :mount
      t.string :type_payment
      t.string :status

      t.timestamps
    end
  end
end
