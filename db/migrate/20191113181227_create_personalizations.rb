class CreatePersonalizations < ActiveRecord::Migration[6.0]
  def change
    create_table :personalizations do |t|
      t.string  :slug
      t.references :runner, foreign_key: true
      t.string  :personalitation
      t.timestamps
    end
  end
end
