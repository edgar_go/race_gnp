class CreateCities < ActiveRecord::Migration[6.0]
  def change
    create_table :cities do |t|
      t.string :slug
      t.string :city
      t.string :name
      t.timestamps
    end

    create_table :cities_promotions do |t|
      t.belongs_to :promotions, index: true

      t.belongs_to :cities, index: true
    end
  end
end
