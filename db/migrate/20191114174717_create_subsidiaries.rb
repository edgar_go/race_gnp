class CreateSubsidiaries < ActiveRecord::Migration[6.0]
  def change
    create_table :subsidiaries do |t|
      t.string  :slug
      t.string  :serial
      t.string  :name
      t.integer :filial_sede_id 
      t.string  :representantive
      t.timestamps
    end

    create_table :subsidiaries_cities do |t|
      t.belongs_to :cities, index: true

      t.belongs_to :subsidiaries, index: true
    end
  end
end
