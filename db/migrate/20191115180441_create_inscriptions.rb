class CreateInscriptions < ActiveRecord::Migration[6.0]
  def change
    create_table :inscriptions do |t|
      t.string  :slug
      t.string  :folio
      t.references :race, foreign_key: true
      t.integer :race_folio
      t.references :runner, foreign_key: true
      t.boolean :terms_and_conditions
      t.timestamps
    end
  end
end
